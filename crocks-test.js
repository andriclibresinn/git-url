console.clear()

// https://crocks.dev/docs/crocks/Result.html
// https://github.com/rametta/pratica#result

const Result = require('crocks/Result')
const and = require('crocks/logic/and')
const bimap = require('crocks/pointfree/bimap')
const composeB = require('crocks/combinators/composeB')
const ifElse = require('crocks/logic/ifElse')
const isNumber = require('crocks/predicates/isNumber')
const liftA2 = require('crocks/helpers/liftA2')

const { Err, Ok } = Result

console.log(Result.of())

// prod :: Number -> Number -> Number
const prod = a => b => a * b

// gte :: Number -> Number -> Boolean
const gte = y => x => x >= y

// lte :: Number -> Number -> Boolean
const lte = y => x => x <= y

// between :: (Number, Number) -> Boolean
const between = (x, y) => and(gte(x), lte(y))

// ensure :: (a -> Boolean) -> a -> Result a
const ensure = pred => ifElse(pred, Ok, Err)

// inRange :: Number -> Result
const inRange = ensure(between(10, 15))

console.log(inRange(12))
//=> Ok 12

console.log(inRange(25))
//=> Err 25

console.log(inRange('Test'))
//=> Err "Test"

// ensureNumber :: a -> Result [a] a
const ensureNumber = composeB(
  bimap(x => [ x ], x => x),
  ensure(isNumber)
)

ensureNumber('Not a number 1').alt(ensureNumber('Not a number 2'))
//=> Err [ "Not a number 1", "Not a number 2" ]

liftA2(
  prod,
  ensureNumber('Not 21'),
  ensureNumber('Not 2')
)
//=> Err [ "Not 21", "Not 2" ]

liftA2(
  prod,
  ensureNumber(21),
  ensureNumber(2)
)
//=> Ok 42